# Organization of Michelson Optimistic Roll-Up

This repository contains material related to the management of
Michelson Optimistic Roll-Up. It serves as an entrypoint to get
information about the organization, the design, and the implementation
of this project.
